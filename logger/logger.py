import asyncio
from aioconsole import AsynchronousCli
import argparse
from functools import partial


class Logger(object):
    def __init__(self):
        self.devices = {}

    def add_device(self, name, device):
        self.devices[name] = device
        device.start()

    def get_last_value(self, channel, device=None):
        if device is None:
            device = list(self.devices.values())[0]

        return device.get_last_value(channel)


@asyncio.coroutine
def socket_callback(logger, reader, writer):
    data = yield from reader.read(10)
    try:
        message = data.decode().split(" ")
        if len(message) == 1:
            data = "%.2f\n" % logger.get_last_value(int(message[0]))
        elif len(message) == 2:
            data = "%.2f\n" % logger.get_last_value(int(message[1]), message[0])
        else:
            data = "Invalid format\n"
    except Exception as e:
        data = str(e)
    writer.write(data.encode())
    yield from writer.drain()
    writer.close()


@asyncio.coroutine
def dump_channels(logger, reader, writer, channel):
    for channel in channels:
        writer.write(("%.2f\n" % logger.get_last_value(channel)).encode())


def make_cli(logger, streams=None):
    parser = argparse.ArgumentParser()
    parser.add_argument("channel", type=int)
    commands = dict(dump=(partial(dump_channels, logger), parser))
    return AsynchronousCli(commands, streams)


if __name__ == "__main__":
    logger = Logger()
    from .driver.tc08 import TC08

    channels = {i: "K" for i in range(1, 9)}
    logger.add_device("TC08", TC08(channels))
    loop = asyncio.get_event_loop()
    server = loop.run_until_complete(
        asyncio.start_server(partial(socket_callback, logger), port=2023)
    )
    # loop.create_task(make_cli(logger).interact())
    try:
        print("Serving")
        loop.run_forever()
    except KeyboardInterrupt:
        print("Shutdown")
    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()
