import atexit
import usbtc08


class TC08Driver(object):
    def __init__(self):
        self._dev = usbtc08.usb_tc08_open_unit()
        atexit.register(usbtc08.usb_tc08_close_unit, self._dev)
        if self._dev == 0:
            raise Exception("Failed to init TC-08: %s" % self._dev)
        self._call(usbtc08.usb_tc08_set_mains, self._dev, 0)  # 50 Hz rejection

    def _call(self, fun, *args):
        ret = fun(*args)
        if ret == 0:
            error = usbtc08.usb_tc08_get_last_error(self._dev)
            raise Exception(error)
        return ret

    def init_channel(self, number, tc_type="K"):
        self._call(usbtc08.usb_tc08_set_channel, self._dev, number, ord(tc_type))

    def start(self, interval=1000):
        return self._call(usbtc08.usb_tc08_run, self._dev, interval)

    def stop(self):
        self._call(usbtc08.usb_tc08_stop, self._dev)

    def get_temp(self, channel):
        buffer_length = usbtc08.USBTC08_MAX_SAMPLE_BUFFER
        temp_buffer = usbtc08.floatArray(buffer_length)
        times_ms_buffer = usbtc08.intArray(buffer_length)
        overflow = usbtc08.shortArray(1)
        samples = self._call(
            usbtc08.usb_tc08_get_temp,
            self._dev,
            temp_buffer,
            times_ms_buffer,
            buffer_length,
            overflow,
            channel,
            usbtc08.USBTC08_UNITS_CENTIGRADE,
            0,
        )

        return [(times_ms_buffer[i], temp_buffer[i]) for i in range(samples)]


class TC08(object):
    def __init__(self, channels):
        self._driver = TC08Driver()

        self._channels = channels
        for num, sensor in self._channels.items():
            self._driver.init_channel(num, sensor)

        self._last_values = {chan: 0 for chan in self._channels}

    def start(self):
        self._driver.start()

    def stop(self):
        self._driver.stop()

    def get_last_value(self, channel):
        times, values = zip(*self._driver.get_temp(channel))

        if values:
            self._last_values[channel] = values[-1]

        return self._last_values[channel]
