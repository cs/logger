from setuptools import find_packages, setup

setup(
    name="logger-tc08",
    author="Hendrik v. Raven",
    packages=find_packages(),
    python_requires='<3.0',
    install_requires=[
        "attrs",
        "usbtc08",
        ],
    dependency_links=[
        "https://github.com/bankrasrg/usbtc08/tarball/master#egg=usbtc08&subdirectory=source",
        ],
    )

